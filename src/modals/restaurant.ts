export interface restaurant {
    id:string,
    district:string;
    name:string,
    user:string,
    type:string,
    min:string,
    max:string,
    visited:boolean;
    opinion:string;
} 