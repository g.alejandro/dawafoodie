import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthenticationService } from '../services/authentication.service';

import { LoadingController } from '@ionic/angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  forgot:String="Forgot your password?";
  signup:String="Sign Up";
  disableInput:boolean=false;
  textMessage:string="";
  textUsername:string = "";
  textPassword:string = "";
  textVacio:string="";
  typepass:string = "password";

  constructor(private router:Router, public alertController:AlertController, private authservices:AuthenticationService, private fb: Facebook,
    public loadingController: LoadingController, private fireAuth: AngularFireAuth
    ) {}
    
    


  

  login(){
    if(this.textUsername.trim().length> 0 && this.textPassword.trim().length > 0){
   this.authservices.signup(this.textUsername,this.textPassword)
      .then(() => {
        this.router.navigateByUrl("/user");
        sessionStorage.setItem("email",this.textUsername);
      }, error => {
        this.authservices.presentToast("Usuario o contraseña incorrectos");
      })
    }else{
        this.textMessage="Formulario Incorrecto";
    } 
 
  }



  email(){
    if(this.textUsername.trim().length> 0){
        this.textMessage="";
    }else{
        this.textMessage="Formulario Incorrecto";
    }
 
  }
  onClickEye(){
    if(this.typepass == "password"){
      this.typepass = "text";
    }else{
      this.typepass = "password";
    }
  }

    password(){
    if(this.textPassword.trim().length> 0){
        this.textMessage="";
    }else{
        this.textMessage="Formulario Incorrecto";
    }
 
  }
}





