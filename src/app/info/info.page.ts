import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthenticationService } from '../services/authentication.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {
  email:string;
  name:string;
  constructor(private activatedRoute: ActivatedRoute, private router:Router,private db:AngularFirestore,  private authservices:AuthenticationService, private fireAuth: AngularFireAuth) { 
  }
  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.email = params['email'];
      this.name = params['name'];
      }
    );
  }

  logout() {
    this.fireAuth.auth.signOut().then(() => {
      this.router.navigateByUrl("/home");
    })
  }

}
