import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { users } from 'src/modals/users';
import { UserService } from './../services/user.service';
import { AuthenticationService } from '../services/authentication.service';
import { AngularFirestore } from '@angular/fire/firestore';



@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  textMail:string = "";
  textName:string = "";
  textPassword:string = "";
  textMessage:string="";
  typepass:string = "password";
  user:users = {
    "name":"",
    "id":"",
    "mail":"",
  } ; 
  constructor(private router:Router, private UserService:UserService ,private authservices:AuthenticationService, private db:AngularFirestore) { }

  ngOnInit() {
  }

  name(){
    if(this.textName.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }


  email(){
    if(this.textMail.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }

  password(){
    if(this.textPassword.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }

  save(){
    if(this.textMail.trim().length> 0 && this.textPassword.trim().length > 0){
    this.router.navigateByUrl("/home");
    this.authservices.createuser(this.textMail,this.textPassword)
      .then(() => {
        this.authservices.presentToast("Usuario creado correctamente");
        this.user.name = this.textName;
        this.user.mail = this.textMail;
        this.UserService.createUser(this.user);
      }, error => {
        this.authservices.presentToast("Error al crear usuario");
      })

      }else{
        this.textMessage="Formulario Incorrecto";
    } 
  }

  onClickEye(){
    if(this.typepass == "password"){
      this.typepass = "text";
    }else{
      this.typepass = "password";
    }
  }

}

