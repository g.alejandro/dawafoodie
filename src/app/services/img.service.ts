import { Injectable } from '@angular/core';
import { img } from 'src/modals/img';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ImgService {

  constructor(private db:AngularFirestore) { }

  createimg(img:img){
    this.db.doc('/imagenes/'+img.id).set({ img });
  }
}
