import { Injectable } from '@angular/core';
import { restaurant } from 'src/modals/restaurant';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  constructor(private db:AngularFirestore) { }


  
  createRestaurant(res:restaurant){
    this.db.doc('/restaurants/'+res.id).set({ res });
  }

  editRestaurant(res:restaurant){
    this.db.doc('/restaurants/'+res.id).update({ res });
  }

  deleteRestaurant(res:restaurant){
    this.db.doc('/restaurants/'+res.id).delete();
  }
}
