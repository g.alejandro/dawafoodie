import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { users } from 'src/modals/users';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private db:AngularFirestore) { }

  createUser(user:users){
    user.id=this.db.createId();
    this.db.doc('/users/'+user.id).set({ user });
  }

}
