import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
 

  constructor(private afAuth:AngularFireAuth, private toastController: ToastController) { }

  async presentToast(messaje:string) {
    const toast = await this.toastController.create({
      message: messaje,
      duration: 2000
    });
    toast.present();
  }

  async createuser(mail:string,pass:string){
    return this.afAuth.auth.createUserWithEmailAndPassword(mail, pass)
    .then((newCredential:firebase.auth.UserCredential) => {
      console.log(newCredential);
    }).catch(error => {
      console.log(error);
      throw new Error(error);
    })
  }

  signup(mail:string,pass:string){
    return this.afAuth.auth.signInWithEmailAndPassword(mail, pass)
    .then((newCredential:firebase.auth.UserCredential) => {
      console.log(newCredential);
    }).catch(error => {
      console.log(error);
      throw new Error(error);
    })
  }

    
  }


