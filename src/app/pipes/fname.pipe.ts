import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fname'
})
export class FnamePipe implements PipeTransform {

  transform(arreglo: any[], texto: string ): any[] {
    
    

    
    if(texto === ''){
      return arreglo;
    }
    texto = texto.toLowerCase();

    return arreglo.filter( item => {
      return item.res.name.toLowerCase()
      .includes( texto );
    });
    
    
  }

}
