import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestoreCollection, AngularFirestore, DocumentData } from '@angular/fire/firestore';
import { User } from 'firebase';
import { users } from 'src/modals/users';
import { AuthenticationService } from '../services/authentication.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { restaurant } from 'src/modals/restaurant';


@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {
  usuario:users[] = [];
  restaurantes:any[] = [];
  images:any[] = [];
  emailRegistrado:string="";
  nombreUser:string = "";
  fnombre:string = "";
  constructor(private router:Router,private db:AngularFirestore,  private authservices:AuthenticationService, private fireAuth: AngularFireAuth) { 
    this.emailRegistrado=sessionStorage.getItem("email");
    this.nombreUser = sessionStorage.getItem("name");
  } 

  

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    console.log(this.emailRegistrado);

    //USERS
    let usersCollection:AngularFirestoreCollection = this.db.collection<users>('users');
    usersCollection.valueChanges().subscribe(
      res=>{
        res.forEach(element => {
          if(element.user.mail == this.emailRegistrado)
            this.nombreUser = element.user.name;
        })})

    //imagenes
    let imgCollection:AngularFirestoreCollection = this.db.collection<restaurant>('img');
    imgCollection.valueChanges().subscribe(
      res=>{
        this.images = res;
        console.log(this.images);
      })

      //RESTAURANTES
    let resCollection:AngularFirestoreCollection = this.db.collection<restaurant>('restaurants');
    resCollection.valueChanges().subscribe(
      res=>{
        this.restaurantes = res;
        let num = 0;
        for(let i of this.restaurantes){
          if(i.res.user != this.emailRegistrado){
            this.restaurantes.splice(num, 1 );
          }
          num++;
        }

      })

    
    }

    addres(){
      this.router.navigateByUrl("/new-restaurant");
    }

    logout() {
      this.fireAuth.auth.signOut().then(() => {
        this.router.navigateByUrl("/home");
      })
    }

    gotores(rest:any){
      console.log(rest);
      if(rest.opinion == undefined){
        rest.opinion="This restaurant have no opinions";
      }
       this.router.navigate(["/restaurant", 
       {id: rest.id, name: rest.name, min:rest.min, max: rest.max, type: rest.type, visited: rest.visited, op: rest.opinion, district: rest.district}, 
      ] );
      
    }

    onRenderItems(event) {
      console.log(`Moving item from ${event.detail.from} to ${event.detail.to}`);
       let draggedItem = this.restaurantes.splice(event.detail.from,1)[0];
       this.restaurantes.splice(event.detail.to,0,draggedItem)
      //this.listItems = reorderArray(this.listItems, event.detail.from, event.detail.to);
      event.detail.complete();
    }

    user(){
      console.log(this.emailRegistrado);
      console.log(this.nombreUser);
      this.router.navigate(["/info", 
       {name: this.nombreUser, email: this.emailRegistrado}, 
      ] );
    }


    //FILTROS

    buscarName( event ){
      this.fnombre = event.detail.value;
    }

}  
