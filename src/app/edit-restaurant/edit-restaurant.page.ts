import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticationService } from '../services/authentication.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { restaurant } from 'src/modals/restaurant';
import { RestaurantService } from '../services/restaurant.service';

@Component({
  selector: 'app-edit-restaurant',
  templateUrl: './edit-restaurant.page.html',
  styleUrls: ['./edit-restaurant.page.scss'],
})
export class EditRestaurantPage implements OnInit {
  emailRegistrado:string="";
  district:string = "";
  id:string;
  name:string;
  min:string;
  max:string;
  type:string;
  visited:boolean=true;
  opinion:string="";
  textMessage:string="";
  imgtick:string="../../assets/icon/green-tick.png";

  res:restaurant = {
    "id":"",
    "name":"",
    "district":"",
    "user":"",
    "type":"",
    "min":"",
    "max":"",
    "visited":false,
    "opinion":"",


  }
  constructor(private ResService:RestaurantService ,private activatedRoute: ActivatedRoute, private router:Router,private db:AngularFirestore,  private authservices:AuthenticationService, private fireAuth: AngularFireAuth) { }
   

  ngOnInit() {
    this.emailRegistrado=sessionStorage.getItem("email");
    this.activatedRoute.params.subscribe((params) => {
      this.id = params['id'];
      this.name = params['name'];
      this.min = params['min'];
      this.max = params['max'];
      this.type = params['type'];
      this.visited = params['visited'];
    }); 
    console.log(this.id);
  }

  txtname(){
    if(this.name.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }
  txtdistrict(){
    if(this.name.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }

  minprice(){
    if(this.name.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }

  maxprice(){
    if(this.name.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }


  tick(){
      if(this.imgtick == "../../assets/icon/black-tick.png"){
      this.visited = true;
        this.imgtick = "../../assets/icon/green-tick.png"
      }else{
      this.visited = false;
      this.opinion="";
      this.imgtick = "../../assets/icon/black-tick.png"
    }
  }
  
  textopinion(){
    if(this.opinion.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }

  update(){
    this.res.id = this.id;
    this.res.user = this.emailRegistrado;  
    this.res.name = this.name;
    this.res.district = this.district;
    this.res.type = this.type;
    this.res.min = this.min;
    this.res.max = this.max;
    this.res.visited = this.visited;
    this.res.opinion = this.opinion;
    console.log(this.res);
    this.ResService.editRestaurant(this.res);
    this.router.navigateByUrl("/user");

  }

}
