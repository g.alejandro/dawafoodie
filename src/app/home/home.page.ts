import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  isLoggedIn = false;
  users = { id: '', name: '', email: '', picture: { data: { url: '' } } };
  constructor(private router:Router, public alertController:AlertController, private authservices:AuthenticationService, private fb: Facebook,
    public loadingController: LoadingController, private fireAuth: AngularFireAuth
    ) {
      fb.getLoginStatus()
  .then(res => {
    console.log(res.status);
    if (res.status === 'connect') {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
  })
  .catch(e => console.log(e));

    }
    


    loading: any;

    async ngOnInit() {
      this.loading = await this.loadingController.create({
        message: 'Connecting ...'
      });
    }


    async presentLoading(loading) {
      await loading.present();
    }
  
  
    async facebook() {
  
      this.fb.login(['email'])
      .then((response: FacebookLoginResponse) => {
        this.onLoginSuccess(response);
        console.log(response.authResponse.accessToken);
      }).catch((error) => {
        console.log(error)
        alert('error:' + error)
      });

  
    }

    onLoginSuccess(res: FacebookLoginResponse) {
      // const { token, secret } = res;
      const credential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
      this.fireAuth.auth.signInWithCredential(credential)
        .then((response) => {
          this.router.navigateByUrl("user");
          this.loading.dismiss();
        })
  
    }

    onLoginError(err) {
      console.log(err);
    }


  
 
    


  login(){
    this.router.navigateByUrl("/login");
  }

  sign(){
    this.router.navigateByUrl("/register");

  }
}
