import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { RestaurantService } from '../services/restaurant.service';
import { AngularFirestoreCollection, AngularFirestore, DocumentData } from '@angular/fire/firestore';
import { User } from 'firebase';
import { users } from 'src/modals/users';
import { AuthenticationService } from '../services/authentication.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { restaurant } from 'src/modals/restaurant';
import { img } from 'src/modals/img';




@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.page.html',
  styleUrls: ['./restaurant.page.scss'],
})
export class RestaurantPage implements OnInit {
  id:string;
  name:string;
  min:string;
  max:string;
  type:string;
  district:string;
  visited:boolean;
  opinion:string="";
  urlimagen:any;
  emailRegistrado:string;
  res:restaurant = {
    "id":"",
    "name":"",
    "district": "",
    "user":"",
    "type":"",
    "min":"",
    "max":"",
    "visited":false,
    "opinion":"",
  }
  constructor(private ResService:RestaurantService ,public alertController:AlertController, private activatedRoute: ActivatedRoute, private router:Router,private db:AngularFirestore,  private authservices:AuthenticationService, private fireAuth: AngularFireAuth) { 
    
  }

  ionViewWillEnter(){
    
   
  }

  ngOnInit() {
    this.emailRegistrado=sessionStorage.getItem("email");
    this.activatedRoute.params.subscribe((params) => {
      this.id = params['id'];
      this.name = params['name'];
      if(params['district'] == undefined || params['district'] == ""){
        this.district="No district";
      }else{

        this.district = params['district'];
      }
      this.min = params['min'];
      this.max = params['max'];
      this.type = params['type'];
      this.visited = params['visited'];
      if(params['op'] == undefined || params['op'] == ""){

        this.opinion="This restaurant have no opinions";
      }else{
        this.opinion = params['op'];
      }
    });

     //imagenes
     let imgCollection:AngularFirestoreCollection = this.db.collection<img>('imagenes');
     imgCollection.valueChanges().subscribe(
       res=>{
         res.forEach(element => { 
           console.log(element);
           if(element.img.idres == this.id)
             this.urlimagen = element.img.url;
            else
              this.urlimagen = "../../assets/icon/logo.png";
         })})
  }

  edit(){
    this.router.navigate(["/edit-restaurant", 
    {id: this.id, name: this.name, min:this.min, max: this.max, type: this.type, visited: this.visited, op: this.opinion, district: this.district}, 
   ] );

  }

  async delete(){
    const alert = await this.alertController.create({
      header: 'Delete',
      subHeader: 'Are you sure to delete?',
      
      buttons: [
        {text: 'Cancel'},
        {text: 'Accept', handler: (event)=> {
          this.res.id = this.id;
          this.res.user = this.emailRegistrado;  
          this.res.name = this.name;
          this.res.type = this.type;
          this.res.min = this.min;
          this.res.max = this.max;
          this.res.visited = this.visited;
          this.res.opinion = this.opinion;
          this.ResService.deleteRestaurant(this.res);
          this.router.navigateByUrl("user");
        }}
      ]
    });
    await alert.present();
    }

  

}
