import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController, ActionSheetController } from '@ionic/angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { AuthenticationService } from '../services/authentication.service';
import { restaurant } from 'src/modals/restaurant';
import { img } from 'src/modals/img';
import { ImgService } from '../services/img.service';

import { RestaurantService } from '../services/restaurant.service';
import { AngularFirestore } from '@angular/fire/firestore';

import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import { AngularFireStorage  } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

 
@Component({
  selector: 'app-new-restaurant',
  templateUrl: './new-restaurant.page.html',
  styleUrls: ['./new-restaurant.page.scss'],
})
export class NewRestaurantPage {
  district:string = "";
  name:string = "";
  type:string = "";
  min:string = "";
  max:string = "";
  imgtick:string="../../assets/icon/black-tick.png";
  visited:boolean = false;
  user:string = "";
  opinion:string="";
  textMessage:string="";
  base64Image:any;
  img:img = {
    "id":"",
    "url":"",
    "idres":"",
  }
  options1:any;
  imageResponse:any;

  res:restaurant = {
    "id":"",
    "name":"",
    "district":"",
    "user":"",
    "type":"",
    "min":"",
    "max":"",
    "visited":false,
    "opinion":"",

  }
  constructor(private ResService:RestaurantService ,private router:Router, public alertController:AlertController, private authservices:AuthenticationService, private fb: Facebook,
    public loadingController: LoadingController, private fireAuth: AngularFireAuth, private imagePicker: ImagePicker, private file:File,
    private storage: AngularFireStorage,private db:AngularFirestore,  private camera: Camera, private actionSheetController: ActionSheetController,
    private imgservice: ImgService,
    ) {
      this.user=sessionStorage.getItem("email");


    }

 

  logout() {
    this.fireAuth.auth.signOut().then(() => {
      this.router.navigateByUrl("/register");
    })
  }

  txtname(){
    if(this.name.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }

  txtdistrict(){
    if(this.name.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }

  minprice(){
    if(this.name.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }

  maxprice(){
    if(this.name.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }


  tick(){
      if(this.imgtick == "../../assets/icon/black-tick.png"){
      this.visited = true;
        this.imgtick = "../../assets/icon/green-tick.png"
      }else{
      this.visited = false;
      this.opinion = "";
      this.imgtick = "../../assets/icon/black-tick.png"
    }
  } 
  textopinion(){
    if(this.opinion.trim().length> 0){
      this.textMessage="";
  }else{
      this.textMessage="Formulario Incorrecto";
  }
  }


  onClickcamera() {
    console.log("button pressed");
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then(async (imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log(this.base64Image);

      const loading = await this.loadingController.create({
        message: 'Guadando foto...'

      });
      await loading.present();
      this.img.id = this.db.createId();
      let route = `/${this.img.id}`;
      const fileRef = this.storage.ref(route);
      const task = fileRef.putString(this.base64Image, 'data_url');
      task.snapshotChanges().pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(url => {
            this.img.url = url;
            alert(this.img.url);
            loading.dismiss();
          });
        })
      ).subscribe();

    }, (err) => {
      alert(err);
      // Handle error
    });;
  }

  


  //CREAR RESTAURANTE

  create(){
    this.res.user = this.user;  
    this.res.name = this.name;
    this.res.district = this.district;
    this.res.type = this.type;
    this.res.min = this.min;
    this.res.max = this.max;
    this.res.visited = this.visited;
    this.res.opinion = this.opinion;
    this.res.id=this.db.createId();
    this.img.idres = this.res.id;
    this.imgservice.createimg(this.img);
    this.ResService.createRestaurant(this.res);
    this.router.navigateByUrl("/user");

  }

}
