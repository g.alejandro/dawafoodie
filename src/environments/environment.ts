// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};
export const firebaseConfig = {
  apiKey: "AIzaSyDiVOFKn4tqXYBxKypgeEK3xLtM0BGINck",
  authDomain: "dawafoodie-4910f.firebaseapp.com",
  databaseURL: "https://dawafoodie-4910f.firebaseio.com",
  projectId: "dawafoodie-4910f",
  storageBucket: "dawafoodie-4910f.appspot.com",
  messagingSenderId: "439700423709",
  appId: "1:439700423709:web:155f361364bf20e5ceb2f4"
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
